<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/6/2018
 * Time: 10:34 AM
 */
$OPERATION_URL = env("OPERATION_URL");
$FINANCE_URL   = env("FINANCE_URL");

$SHIPPER_URL    = env("SHIPPER_URL");

return [
    "internal_user_app" =>[
        $OPERATION_URL."/saml2/metadata" => [
            "name"      => "operation",
            "login"     => $OPERATION_URL."/saml2/acs",
            "logout"    => $OPERATION_URL."/saml2/sls",
        ],
        $FINANCE_URL."/saml2/metadata" => [
            "name"      => "finance",
            "login"     => $FINANCE_URL."/saml2/acs",
            "logout"    => $FINANCE_URL."/saml2/sls",
        ]
    ],
    "common_user_app"  => [
        $SHIPPER_URL."/saml2/metadata" => [
            "name"      => "shipper",
            "login"     => $SHIPPER_URL."/saml2/acs",
            "logout"    => $SHIPPER_URL."/saml2/sls",
        ],
    ]
//    "internal_app_client_id"    => env('INTERNAL_APP_CLIENT_ID'),
//    "internal_app_secret_id"    => env('INTERNAL_APP_SECRET_ID'),
//    "access_token_request_endpoint" => env('SERVICE_AUTH_ACCESS_TOKEN_ENDPOINT')
];