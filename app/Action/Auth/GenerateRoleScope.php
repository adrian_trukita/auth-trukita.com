<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/18/2018
 * Time: 1:17 PM
 */

namespace App\Action\Auth;


use App\Action\ActionContract;

/**
 * Class GenerateRoleScope
 * @package App\Action\Auth
 */
class GenerateRoleScope extends ActionContract
{
    protected $role;
    public function __construct($role)
    {
        $this->role = $role;
    }

    /**
     * Generate Scope
     * @return array
     */
    public function run()
    {
        // TODO: Implement run() method.

        if($this->role === "admin")
        {
            $scope = ["*"];
        }
        elseif($this->role === "operation")
        {
            $scope = ["*"];
        }
        elseif($this->role === 'arm')
        {
            $scope = ["*"];
        }
        elseif ($this->role === "finance")
        {
            $scope = ["*"];
        }
        elseif($this->role === "3rd_party_app"){
            // 3rd party app just for test
            $scope = ['request-app-resource','route-prices'];
        }
        else
        {
            $scope = [];
        }


        return $scope;
    }

}