<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/11/2018
 * Time: 5:39 PM
 */

namespace App\Action\Auth;


use App\Action\ActionContract;

use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use App\ObjectClass\SPConfig;
use Illuminate\Http\Request;
use LightSaml\Model\Protocol\AuthnRequest;

class BuildLoginSAMLResponse extends ActionContract
{

    protected $authnRequest;
    protected $user;
    protected $issuer_entity_id;

    public function __construct(AuthnRequest $authnRequest, BaseUserInterface $user, $issuer_entity_id)
    {
        $this->authnRequest = $authnRequest;
        $this->user         = $user;

        /** @var SPConfig $spConfig */
        $this->issuer_entity_id     = $issuer_entity_id;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $app_identity   = $this->user->getIdentity();

        $spConfig = new SPConfig($app_identity,$this->issuer_entity_id);
        if(!$spConfig->isExist()){
            throw new \Exception("SP Config not exist");
        }

        // get saml config
        $destination = $spConfig->getLoginDestination();
        $issuer      = $spConfig->getIssuer();
        $cert_path   = $spConfig->getCertPath();
        $key         = $spConfig->getKey();

//        $destination = config('saml.sp.'.base64_encode($authnRequest->getAssertionConsumerServiceURL()).'.destination');
//        $issuer = config('saml.sp.'.base64_encode($authnRequest->getAssertionConsumerServiceURL()).'.issuer');
//        $cert = config('saml.sp.'.base64_encode($authnRequest->getAssertionConsumerServiceURL()).'.cert');
//        $key = config('saml.sp.'.base64_encode($authnRequest->getAssertionConsumerServiceURL()).'.key');

//        $cert   = storage_path('app/sso.cert');
//        $key    = file_get_contents(storage_path('app/sso.key'));
        $certificate = \LightSaml\Credential\X509Certificate::fromFile($cert_path);
        $privateKey = \LightSaml\Credential\KeyHelper::createPrivateKey($key, 'trukita_certs', false);


        $response = new \LightSaml\Model\Protocol\Response();
        $response
            ->addAssertion($assertion = new \LightSaml\Model\Assertion\Assertion())
            ->setID(\LightSaml\Helper::generateID())
            ->setIssueInstant(new \DateTime())
            ->setDestination($destination)
            ->setIssuer(new \LightSaml\Model\Assertion\Issuer($issuer))
            ->setStatus(new \LightSaml\Model\Protocol\Status(new \LightSaml\Model\Protocol\StatusCode('urn:oasis:names:tc:SAML:2.0:status:Success')))
            ->setSignature(new \LightSaml\Model\XmlDSig\SignatureWriter($certificate, $privateKey));

        $email = $this->user->email;
        $name = $this->user->name;

        $assertion
            ->setId(\LightSaml\Helper::generateID())
            ->setIssueInstant(new \DateTime())
            ->setIssuer(new \LightSaml\Model\Assertion\Issuer($issuer))
            ->setSubject(
                (new \LightSaml\Model\Assertion\Subject())
                    ->setNameID(new \LightSaml\Model\Assertion\NameID(
                        $email,
                        \LightSaml\SamlConstants::NAME_ID_FORMAT_EMAIL
                    ))
                    ->addSubjectConfirmation(
                        (new \LightSaml\Model\Assertion\SubjectConfirmation())
                            ->setMethod(\LightSaml\SamlConstants::CONFIRMATION_METHOD_BEARER)
                            ->setSubjectConfirmationData(
                                (new \LightSaml\Model\Assertion\SubjectConfirmationData())
                                    ->setInResponseTo($this->authnRequest->getId())
                                    ->setNotOnOrAfter(new \DateTime('+1 MINUTE'))
                                    ->setRecipient($this->authnRequest->getAssertionConsumerServiceURL())
//                                    ->setRecipient($issuer_entity_id)
                            )
                    )
            )
            ->setConditions(
                (new \LightSaml\Model\Assertion\Conditions())
                    ->setNotBefore(new \DateTime())
                    ->setNotOnOrAfter(new \DateTime('+1 MINUTE'))
                    ->addItem(
                        new \LightSaml\Model\Assertion\AudienceRestriction([$this->authnRequest->getAssertionConsumerServiceURL()])
//                        new \LightSaml\Model\Assertion\AudienceRestriction([$issuer_entity_id,$destination])
                    )->addItem(
                        new \LightSaml\Model\Assertion\AudienceRestriction($this->authnRequest->getIssuer()->getValue())
//                        new \LightSaml\Model\Assertion\AudienceRestriction([$issuer_entity_id,$destination])
                    )
            )
            ->addItem(
                (new \LightSaml\Model\Assertion\AttributeStatement())
                    ->addAttribute(new \LightSaml\Model\Assertion\Attribute(
                        \LightSaml\ClaimTypes::EMAIL_ADDRESS,
                        $email
                    ))
                    ->addAttribute(new \LightSaml\Model\Assertion\Attribute(
                        \LightSaml\ClaimTypes::COMMON_NAME,
                        $name
                    ))
            )
            ->addItem(
                (new \LightSaml\Model\Assertion\AuthnStatement())
                    ->setAuthnInstant(new \DateTime('-10 MINUTE'))
                    ->setSessionIndex('_some_session_index')
                    ->setAuthnContext(
                        (new \LightSaml\Model\Assertion\AuthnContext())
                            ->setAuthnContextClassRef(\LightSaml\SamlConstants::AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT)
                    )
            )
        ;

        return $this->sendSAMLResponse($response);
    }

    protected function sendSAMLResponse($response){
        $bindingFactory = new \LightSaml\Binding\BindingFactory();
        $postBinding = $bindingFactory->create(\LightSaml\SamlConstants::BINDING_SAML2_HTTP_POST);
        $messageContext = new \LightSaml\Context\Profile\MessageContext();
        $messageContext->setMessage($response)->asResponse();

        /** @var \Symfony\Component\HttpFoundation\Response $httpResponse */
        $httpResponse = $postBinding->send($messageContext);

        return $httpResponse->getContent();
    }
}