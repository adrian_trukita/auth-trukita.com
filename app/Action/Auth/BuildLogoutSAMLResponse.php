<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/31/2018
 * Time: 5:04 PM
 */

namespace App\Action\Auth;

use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use App\ObjectClass\SPConfig;
use LightSaml\Model\Protocol\LogoutRequest;
use LightSaml\Model\Protocol\LogoutResponse;

class BuildLogoutSAMLResponse
{
    protected $logoutRequest;
    protected $user;

    public function __construct(LogoutRequest $logoutRequest, BaseUserInterface $user)
    {
        $this->logoutRequest    = $logoutRequest;
        $this->user             = $user;
    }

    public function run()
    {
        // get all saml config
        $user_app_identity = $this->user->getIdentity();
        $all_saml_config = config("saml-config")[$user_app_identity];
        $target_url = [];

        foreach ($all_saml_config as $entity_id => $param){
            $spConfig = new SPConfig($user_app_identity, $entity_id);

            $destination = $spConfig->getLogoutDestination();
            $issuer      = $spConfig->getIssuer();
            $cert_path   = $spConfig->getCertPath();
            $key         = $spConfig->getKey();

            $certificate = \LightSaml\Credential\X509Certificate::fromFile($cert_path);
            $privateKey = \LightSaml\Credential\KeyHelper::createPrivateKey($key, 'trukita_certs', false);

            $response = new LogoutResponse();
            $response
                ->setID(\LightSaml\Helper::generateID())
                ->setIssueInstant(new \DateTime())
                ->setDestination($destination)
                ->setIssuer(new \LightSaml\Model\Assertion\Issuer($issuer))
                ->setStatus(new \LightSaml\Model\Protocol\Status(new \LightSaml\Model\Protocol\StatusCode('urn:oasis:names:tc:SAML:2.0:status:Success')))
                ->setSignature(new \LightSaml\Model\XmlDSig\SignatureWriter($certificate, $privateKey));

            $target_url[] = $this->getRequestUrl($response);
        }
        return $target_url;

    }

    private function getRequestUrl($response){
        $bindingFactory = new \LightSaml\Binding\BindingFactory();
        $postBinding = $bindingFactory->create(\LightSaml\SamlConstants::BINDING_SAML2_HTTP_REDIRECT);
        $messageContext = new \LightSaml\Context\Profile\MessageContext();

        $messageContext->setMessage($response)->asResponse();


        /** @var \Symfony\Component\HttpFoundation\Response $httpResponse */
        $httpResponse = $postBinding->send($messageContext);
        $targetUrl = $httpResponse->getTargetUrl();
        return $targetUrl;
    }
}