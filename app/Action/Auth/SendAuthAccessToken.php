<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/11/2018
 * Time: 6:05 PM
 */

namespace App\Action\Auth;


use App\Action\ActionContract;
use GuzzleHttp\Client;

/**
 * @deprecated
 * Class SendAuthAccessToken
 * @package App\Action\Auth
 */
class SendAuthAccessToken extends ActionContract
{
    protected $target_auth;
    protected $username;
    protected $password;
    protected $client_id;
    protected $client_secret;

    public function __construct($target_auth, $username, $password, $client_id, $client_secret)
    {
        $this->target_auth  = $target_auth;
        $this->username     = $username;
        $this->password     = $password;
        $this->client_id    = $client_id;
        $this->client_secret = $client_secret;
    }

    public function run(){
        $param_request_get_token['form_params']= [
            'grant_type'    => "password",
            "client_id"     => $this->client_id,
            "client_secret" => $this->client_secret,
            'username' => $this->username,
            'password' => $this->password
        ];
        $param_request_get_token['headers'] = ['Content-Type' => 'application/x-www-form-urlencoded','cache-control' => 'no-cache'];
        $client = new Client();

        // get access token
        $send_request = $client->post(route('passport.token'),$param_request_get_token);

        $response = $send_request->getBody()->getContents();
        dump($response);

        $send_token_request_response['form_params'] = [
            'token_request_response' => base64_encode($response)
        ];
        $send_token_request_response['headers'] = ['Content-Type' => 'application/x-www-form-urlencoded','cache-control' => 'no-cache'];

//        $iframe_data['url'] = $this->target_auth."?token_request_response=".base64_encode($response);
//        return view('iframe-http',$iframe_data)->render();

//        $send_request2 = $client->get($this->target_auth,$send_token_request_response);

        dump($this->target_auth);
        $send_request2 = $client->get($this->target_auth."?token_request_response=".base64_encode($response));
        $response2 = $send_request2->getBody()->getContents();
        dd($response2);
//        echo $response;
//        die();
//        return $this->target_auth."?token_request_response=".base64_encode($response);
    }
}