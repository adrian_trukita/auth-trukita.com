<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:49 PM
 */

namespace App\Action\Auth;


use App\Action\ActionContract;
use App\Action\Auth\Contract\doLoginContract;
use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use App\Repositories\Contract\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class DoLogin extends ActionContract implements doLoginContract
{
    protected $email;
    protected $password;
    protected $providerRepository;

    public function __construct($email, $password,$provider="users")
    {
        $this->email    = $email;
        $this->password = $password;

        $config = config('auth.providers.'.$provider);


        $repository = $config['repository'];

        /**
         * @var $repositoryClass UserRepositoryInterface
         */
        $repositoryClass = new $repository;
        $this->providerRepository   = $repositoryClass;
    }

    /**
     * @return BaseUserInterface|bool|null
     */
    public function run(){
        $param['email'] = $this->email;
        $user_data = $this->providerRepository->getAllUser($param);
        if($user_data->count()){
            $user_data = $user_data->get(0);
            // check password
            $hashedPassword = $user_data->password; // this is same with bcrypt
            if (Hash::check($this->password, $hashedPassword)){
                return $user_data;
            }
            return false;
        }
        return null;
    }
}