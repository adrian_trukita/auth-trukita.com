<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/27/2018
 * Time: 1:59 PM
 */

namespace App\Action\Auth;


use App\Action\ActionContract;
use App\Model\Contract\BaseInterfaces\BaseUserInterface;

class GenerateUserToken extends ActionContract
{
    protected $user;
    protected $name;
    protected $scope;

    public function __construct(BaseUserInterface $user, $name='InternalAppToken',$scope=null)
    {
        $this->user = $user;
        $this->name = $name;

        // generate scope
        $generateScope = new GenerateRoleScope($this->user['role']);
        $defined_scope = $generateScope->run();

        $this->scope = ($scope)?$scope:$defined_scope;
    }

    public function run(){
        $pa_token = $this->user->createToken($this->name,$this->scope);
        $token = [
            "access_token" => $pa_token->accessToken,
            "id"    => $pa_token->token->id,
            "expires_at"    => $pa_token->token->expires_at
        ];


        return $token;
    }
}