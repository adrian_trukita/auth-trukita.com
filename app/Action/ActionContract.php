<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:50 PM
 */

namespace App\Action;


abstract class ActionContract
{
    public abstract function run();
}