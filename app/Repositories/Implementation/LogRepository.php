<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 9:43 PM
 */

namespace App\Repositories\Implementation;


use App\Model\Logs;
use App\Repositories\Contract\LogRepositoryInterface;

class LogRepository extends LogRepositoryInterface
{
    public function __construct()
    {
        $logsModel  = new Logs();
        parent::__construct($logsModel);
    }

    /**
     * @param array $add_param
     * @return mixed
     * @throws \Exception
     */
    public function addLogs(array $add_param){
        $in_array = ["type","actor_class","actor_identifier","onFunction","description"];

        foreach ($in_array as $key){
            if(!array_key_exists($key,$add_param)){
                throw new \Exception("Please provide ".$key."into logs");
            }
        }

        $this->model->type          = $add_param['type'];
        $this->model->actor_class   = $add_param['actor_class'];
        $this->model->actor_identifier = $add_param['actor_identifier'];
        $this->model->onFunction        = $add_param['onFunction'];
        $this->model->description   = $add_param['description'];

        // php 7.0 null coalescence
        $this->model->isJson        = $add_param['isJson'] ?? null;
        $this->model->expectsJson   = $add_param['expectsJson'] ?? null;
        $this->model->wantsJson     = $add_param['wantsJson'] ?? null;
        $this->model->header        = $add_param['header'] ?? null;
        $this->model->querystring   = $add_param['querystring'] ?? null;
        $this->model->method        = $add_param['method'] ?? null;
        $this->model->ip            = $add_param['ip'] ?? null;
        $this->model->ips           = $add_param['ips'] ?? null;
        $this->model->userAgent     = $add_param['userAgent'] ?? null;
        $this->model->fingerprint   = $add_param['fingerprint'] ?? null;

        $log_id = $this->doSave($this->model);
        return $log_id;
    }
}