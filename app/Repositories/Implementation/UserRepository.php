<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:22 PM
 */

namespace App\Repositories\Implementation;


use App\Model\Users;
use App\Models\Contract\UserInterface;
use App\Repositories\Contract\UserRepositoryInterface;
use Illuminate\Support\Facades\Log;

class UserRepository extends UserRepositoryInterface
{

    public function __construct()
    {
        $userModels = new Users();
        parent::__construct($userModels);
    }

    /**
     * Set Activation key to user
     * @param UserInterface $user
     * @param $activation_key
     * @return bool
     */
    public function setActivationKey(UserInterface $user, $activation_key){
        $update['activation_key']   = $activation_key;
        try{
            $this->doUpdate($update, $user);
        }catch (\Exception $e){
            Log::error(loggingExceptionString($e));
            return false;
        }
       return true;
    }

    /**
     * @param $activation_key
     * @param null $additional_attribute
     * @return Users\Illuminate\Database\Eloquent\Model|null
     */
    public function getUserByActivationKey($activation_key, $additional_attribute = null){
        $user_data = $this->doGet("all",["activation_key" => $activation_key],$additional_attribute);
        if($user_data->count()){
            $user_data = $user_data->get(0);
        }else{
            $user_data = null;
        }
        return $user_data;
    }


}