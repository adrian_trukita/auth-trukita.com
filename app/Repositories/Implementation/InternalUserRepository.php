<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:22 PM
 */

namespace App\Repositories\Implementation;


use App\Model\InternalUsers;
use App\Repositories\Contract\InternalUserRepositoryInterface;
use App\Repostories\RepositoryParent;
use Illuminate\Database\Eloquent\Model;

class InternalUserRepository extends InternalUserRepositoryInterface
{

    public function __construct()
    {
        $userModels = new InternalUsers();
        parent::__construct($userModels);
    }
}