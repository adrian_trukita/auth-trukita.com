<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/5/2018
 * Time: 10:19 AM
 */

namespace App\Repostories;
use Illuminate\Database\Eloquent\Model;


abstract class RepositoryParent
{
    protected $model;
    protected $primary_key;
    protected $select_fields;
    protected $default_order;

    /**
     * RepositoryParent constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model    = $model;
        $this->primary_key  = $this->model->getKeyName();
        $this->select_fields   = "*";
        $this->default_order   = null;
    }

    protected function setFields(array $select_fields){
        $this->select_fields   = $select_fields;
    }
    protected function setDefaultOrder(array $default_order){
        $this->default_order   = $default_order;
    }

    /**
     * @param $data_id
     * @param null $column_key_pair
     * @param null $additional_attribute
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    protected function doGet($data_id, $column_key_pair=null, $additional_attribute= null)
    {
        $modelData = $this->model;

        $modelData = $modelData->select($this->select_fields);
        if($column_key_pair){
            if(array_key_exists('with_trashed',$column_key_pair)){
                $modelData = $modelData->withTrashed();
                unset($column_key_pair['with_trashed']);
            }
            foreach ($column_key_pair as $column_key => $column_value){
                $column_key_logic = explode(' ',$column_key);
                if (sizeof($column_key_logic) !== 1){
                    if(is_array($column_value)){
                        $modelData = $modelData->whereNotIn($column_key_logic[0],$column_value);
                    }else{
                        $modelData = $modelData->where($column_key_logic[0],$column_key_logic[1],$column_value);
                    }

                }else{
                    if(is_array($column_value)){
                        $modelData = $modelData->whereIn($column_key, $column_value);
                    }elseif(is_bool($column_value)){
                        if($column_value){
                            $modelData = $modelData->whereNotNull($column_key);
                        }else{
                            $modelData = $modelData->whereNull($column_key);
                        }
                    }
                    else{
                        $modelData = $modelData->where($column_key,'=',$column_value);
                    }
                }

            }
        }

        // additional attribute
        // handle eager loading
        if($additional_attribute && isset($additional_attribute["relationship_method"])){
            foreach ((array)$additional_attribute["relationship_method"] as $method ){
                $modelData = $modelData->with($method);
            }
        }

        $order = [];
        if($this->default_order){
            $order = $this->default_order;
        }
        if($additional_attribute && isset($additional_attribute["order_by"])){
            $order = array_merge($order, $additional_attribute["order_by"]);
        }
        if(sizeof($order)){
            foreach ($order as $key_order => $key_value ){
                $modelData = $modelData->orderBy($key_order, $key_value);
            }
        }

        if($data_id ==="all"){
            $modelData   = $modelData->get();
        }else{
            if(!is_array($data_id)){
                $modelData = $modelData->where($this->primary_key,$data_id)->first();
            }else{
                $modelData = $modelData->whereIn($this->primary_key, $data_id)->get();

            }
        }

        return $modelData;
    }

    /**
     * @param Model $data
     * @return mixed
     */
    protected function doSave(Model $data){
        $data->save();
        return $data->{$this->primary_key};
    }

    /**
     * @param array $data
     * @param Model|int $primary_id
     * @return mixed
     */
    protected function doUpdate(array $data, $primary_id){
        if(is_int($primary_id)){
            $model_data = $this->model->find($primary_id);
        }else{
            $model_data = $primary_id;
        }

        foreach ($data as $key => $req){
            $model_data->{$key}  = $req;
        }
        return $this->doSave($model_data);
    }

    /**
     * @param array $primary_id
     */
    protected function doDelete(array $primary_id){
        $get_data = $this->model->get($primary_id);
        $get_data->delete();
    }
}