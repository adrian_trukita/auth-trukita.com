<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:20 PM
 */

namespace App\Repositories\Contract;


use App\Model\Contract\BaseInterfaces\BaseUserRepositoryInterface;

abstract class InternalUserRepositoryInterface extends BaseUserRepositoryInterface
{

}