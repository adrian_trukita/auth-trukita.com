<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 3:13 PM
 */

namespace App\Model\Contract\BaseInterfaces;



use App\Repostories\RepositoryParent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

abstract class BaseUserRepositoryInterface extends RepositoryParent
{
    /**
     * Get all user
     * @param null $where_param
     * @param null $additional_attribute
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllUser($where_param = null, $additional_attribute = null){
        $user_list = $this->doGet("all",$where_param,$additional_attribute);

        return $user_list;
    }

    /**
     * Get User by user id
     * @param $user_id
     * @param null $additional_attribute
     * @return BaseUserInterface|Model|null
     */
    public function getUserById($user_id, $additional_attribute = null){
        $user_data = $this->doGet($user_id,null,$additional_attribute);

        return $user_data;
    }

    /**
     * Get user by user email
     * @param $email
     * @param null $additional_attribute
     * @return BaseUserInterface|Model|null
     */
    public function getUserByEmail($email, $additional_attribute = null){
        $user_data = $this->doGet("all",["email" => $email],$additional_attribute);
        if($user_data->count()){
            $user_data = $user_data->get(0);
        }else{
            $user_data = null;
        }
        return $user_data;
    }

    /**
     * @param BaseUserInterface $user
     * @param $new_password
     * @throws \Exception
     */
    public function setNewPassword(BaseUserInterface $user, $new_password){
        try{
            $update['password']         = bcrypt($new_password);
            $update['activation_key']   = "";
            $this->doUpdate($update,$user);
        }catch (\Exception $e){
            Log::error(loggingExceptionString($e));
            throw new \Exception("Update Password Failed");
        }



    }
}