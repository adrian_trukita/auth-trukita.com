<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 10:00 PM
 */

namespace App\Repositories\Contract;


use App\Repostories\RepositoryParent;

abstract class LogRepositoryInterface extends RepositoryParent
{
    abstract  function addLogs(array $add_param);
}