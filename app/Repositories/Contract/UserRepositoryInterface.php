<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:20 PM
 */

namespace App\Repositories\Contract;


use App\Model\Contract\BaseInterfaces\BaseUserRepositoryInterface;
use App\Models\Contract\UserInterface;
use App\Repostories\RepositoryParent;
use Illuminate\Database\Eloquent\Model;

abstract class UserRepositoryInterface extends BaseUserRepositoryInterface
{
    abstract function setActivationKey(UserInterface $user, $activation_key);
    abstract function getUserByActivationKey($activation_key, $additional_attribute = null);
}