<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 3:20 PM
 */

namespace App\Http\Controllers\Auth\Internal;

use App\Action\Auth\BuildLoginSAMLResponse;
use App\Action\Auth\DoLogin;
use App\Http\Controllers\Auth\Contracts\LoginControllerContract;
use App\ObjectClass\SPConfig;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use LightSaml\Model\Protocol\AuthnRequest;
use Saml2;

class LoginController extends LoginControllerContract
{

    public function __construct()
    {
        $this->setUserProviderRepository("internal_users");
    }


    /**
     * Login via web either it's using SAML or personal use
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request){
        $data = [];
        if($request->has('SAMLRequest')){
            $samlRequest = $request->get('SAMLRequest');
            $data['samlRequest'] = $samlRequest;
        }
        return view('auth.login-internal',$data);
    }

    /**
     * Endpoint for another internal web Login with SAML
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function directCheck(Request $request){
        // save temporary email and password
        $auth_guard = Auth::guard('admin');
        if($auth_guard->check()){
            $this->user = $auth_guard->user();
            return $this->handle($request);
        }else{
            return redirect(route('auth.internal.login',["SAMLRequest" => $request->get("SAMLRequest")]));
        }
    }

    /**
     * @param Request $request ["email", "password"] if $this->user not exist yet
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function handle(Request $request){
        if(!$this->user){
            $email = $request->email;
            $password = $request->password;
            $user   = $this->checkLogin($email,$password);
            if($user){
                $this->user = $user;
                Auth::guard('admin')->loginUsingId($user->id);
                session()->put('auth_admin_user',Auth::guard('admin')->user());
            }
        }
        $SAML = $request->SAMLRequest;
        try{
            $callback = $this->redirectLoginSAMLRequest($SAML);
        }catch (\Exception $e){
            return redirect(route('auth.internal.login',["SAMLRequest" => $SAML, "error_msg"=>$e->getMessage()]));
        }

        return $callback;
    }



}