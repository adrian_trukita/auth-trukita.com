<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/27/2018
 * Time: 1:48 PM
 */

namespace App\Http\Controllers\Auth\Internal;


use App\Action\Auth\DoLogin;
use App\Action\Auth\GenerateRoleScope;
use App\Action\Auth\GenerateUserToken;
use App\Http\Controllers\Auth\Contracts\LoginControllerContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalLoginController extends LoginControllerContract
{
    public function __construct()
    {
        $this->setUserProviderRepository("internal_users");
    }

    function handle(Request $request)
    {
        // TODO: Implement handle() method.
        if(! Auth::guard('admin')->check()){
            $email = $request->email;
            $password = $request->password;
            $user    = $this->checkLogin($email,$password);

            if($user){
                Auth::guard('admin')->loginUsingId($user->id);
                session()->put('auth_admin_user',Auth::guard('admin')->user());

                $generateUserTokenAction = new GenerateUserToken($user);
                $token = $generateUserTokenAction->run();
                session()->put('internal_access_token',$token);
                return redirect("/dashboard-panel");

            }else{
                return redirect(route('auth.internal.login',["error_msg"=>"Credential is not valid"]));
            }
        }else{
            return redirect("/internal-dashboard-panel");
        }
    }

}