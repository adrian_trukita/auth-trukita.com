<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/27/2018
 * Time: 1:52 PM
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalLogoutController extends Controller
{
    function handle(Request $request)
    {
        // TODO: Implement handle() method.
        Auth::logout();
        session()->flush();
        return redirect(route('auth.login'));
    }

}