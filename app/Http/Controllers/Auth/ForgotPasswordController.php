<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPassword\ForgotPasswordRequestValidation;
use App\Notifications\SendResetPassword;
use App\Repositories\Contract\UserRepositoryInterface;
use App\Traits\CanLog;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordController extends Controller
{
    use CanLog;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected $userRepository;

    /**
     * Create a new controller instance.
     * ForgotPasswordController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->middleware('guest');
        $this->userRepository   = $userRepository;
    }

    public function view(Request $request)
    {
        return view('auth.forget-password');
    }

    /**
     * Handle forget password request
     * @param ForgotPasswordRequestValidation $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle(ForgotPasswordRequestValidation $request)
    {
        $email = $request->email;
        $user = $this->userRepository->getUserByEmail($email,['status' => 'active']);
        if(!$user):
            return redirect()->back()->withErrors('Account not found');
        endif;

        $activation_key = str_random(32);
        $this->userRepository->setActivationKey($user,$activation_key);

        /* notify to email */
        $when = Carbon::now()->addSeconds(5);
        $user->notify( (new SendResetPassword($user->activation_key,$user->name))->delay($when) );

        session()->flash('message', "Reset password for your account successfully, please check your email for create new password");

        $user_class = get_class($user);

        $this->createLog("Forgot Password Request",__METHOD__,$email.' trying to request forget password sequence ',$request,$user_class,$user->id);

        return redirect(route('auth.login'));
    }


}
