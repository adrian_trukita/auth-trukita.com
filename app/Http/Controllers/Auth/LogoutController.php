<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\Contracts\LoginControllerContract;
use App\Http\Controllers\Auth\Contracts\LogoutControllerContract;
use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use App\ObjectClass\SPConfig;
use GuzzleHttp\Client;
use function GuzzleHttp\Promise\unwrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\TokenRepository;
use LightSaml\Model\Protocol\LogoutRequest;
use LightSaml\Model\Protocol\LogoutResponse;
use Saml2;

class LogoutController extends LogoutControllerContract
{
    protected $tokenRepository;

    public function __construct(TokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    protected function setLogoutUser(BaseUserInterface $user)
    {
        // TODO: Implement setLogoutUser() method.
        $this->user = $user;
    }


    public function handle(Request $request){
        $auth_guard = Auth::guard('web');
        $this->setLogoutUser($auth_guard->user());
        if($request->has('SAMLRequest')){
            $SAML = $request->SAMLRequest;
            try{
                $callback = $this->redirectLogoutSAMLRequest($SAML);

                // revoke accessToken
                $accessToken = session()->get('access_token');
                if($accessToken){
                    $tokenId = $accessToken['id'];
                    $token = $this->tokenRepository->findForUser($tokenId, $auth_guard->id());
                    if (!is_null($token)) {
                        $token->revoke();
                    }
                }

                return $callback;
            }catch (\Exception $e){
                Log::error($e->getMessage());
            }
        }

    }






//    protected function sendSAMLResponse($target_url){
//        $client = new Client();
//        $promises = [];
//        foreach ((array) $target_url as $url){
//            $promises[] = $client->getAsync($url);
////            $client->get($url);
//        }
//
////        dump($target_url);
//        try{
//            $results = unwrap($promises);
//        }catch (\Exception $e){
//            Log::info($e->getMessage());
//        }
//
////        $results = unwrap($promises);
//        return $results;
//    }
}
