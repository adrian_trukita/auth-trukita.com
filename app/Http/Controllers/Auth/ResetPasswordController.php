<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPassword\ChangePasswordRequestValidation;
use App\Repositories\Contract\UserRepositoryInterface;
use App\Traits\CanLog;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    use CanLog;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $userRepository;
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * ResetPasswordController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->middleware('guest');
        $this->userRepository   = $userRepository;
    }

    /**
     * Get forget password confirmation view
     * @param Request $request
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request, $token)
    {
//        dump("test");
//        dd($request->all());
        if(Auth::check()):
            return redirect("/");
        endif;

        $user 	= $this->userRepository->getUserByActivationKey($token);
        if(empty($user)):
            return redirect('error', '404');
        endif;
        $email  = $user->email;
        $user_class = get_class($user);

        $this->createLog("User Reset Password Confirmation",__METHOD__,$email.' attempt to reset password confirmation ',$request,$user_class,$user->id);

        return view('auth.reset-password-confirmation');
    }

    /**
     *
     * @param ChangePasswordRequestValidation $request
     * @param $token
     * @return mixed
     */
    public function handle(ChangePasswordRequestValidation $request, $token){
        try{
            $user 	= $this->userRepository->getUserByActivationKey($token);
            if(empty($user)):
                return redirect('error', '404');
            endif;

            $email = $user->email;
            $password = $request->password;
            $user_class = get_class($user);

            $this->userRepository->setNewPassword($user,$password);
            $this->createLog("User Reset Password Success",__METHOD__,$email.' has changed password successfully',$request,$user_class,$user->id);

        }catch (\Exception $e){
            $this->createLog("User Reset Password Failed",__METHOD__,$email.' has failed to chang password',$request,$user_class,$user->id);
            return redirect()->back()->with('error_message',"Something unexpected occured when updating your password");
        }

        return redirect(route('auth.login'))->with('success_message',"Your password has been changed, go try login with your new password");
    }
}
