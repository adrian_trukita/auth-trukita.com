<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/13/2018
 * Time: 5:02 PM
 */

namespace App\Http\Controllers\Auth;


use App\Action\Auth\generateRoleScope;
use App\Action\Auth\GenerateUserToken;
use App\Http\Controllers\Controller;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccessTokenController extends Controller
{
    /**
     * Serve callback to specified route with access token as response
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function serve(Request $request){
        // please add some identity to make sure the sender is valid
        $return_token = base64_encode(json_encode(null));
        $callback = $request->callback;

        $auth_guard = Auth::guard('web');
        $token_session = session()->get('access_token');
        $currentDate = new \DateTime();

        if($token_session
            && (new \DateTime($token_session['expires_at']) < $currentDate ))
        {
            $return_token = base64_encode(json_encode($token_session));
        }

        elseif($auth_guard->check()){
            $user_data = $auth_guard->user();

            // generate token with scope
            $generateUserTokenAction = new GenerateUserToken($user_data);
            $token = $generateUserTokenAction->run();
            $return_token = base64_encode(json_encode($token));
            session()->put('access_token',$token);
        }

        $callback = $callback."?response=".$return_token;

        return redirect($callback);
    }


}