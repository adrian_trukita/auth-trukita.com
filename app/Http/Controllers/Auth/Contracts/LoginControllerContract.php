<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/6/2018
 * Time: 11:11 AM
 */

namespace App\Http\Controllers\Auth\Contracts;


use App\Action\Auth\BuildLoginSAMLResponse;
use App\Action\Auth\BuildLogoutSAMLResponse;
use App\Action\Auth\DoLogin;
use App\Http\Controllers\Controller;
use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use App\ObjectClass\SPConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LightSaml\Model\Protocol\AuthnRequest;
use LightSaml\Model\Protocol\LogoutRequest;

abstract class LoginControllerContract extends Controller
{
    /**
     * @var $user BaseUserInterface
     */
    protected $user;

    /**
     * @var SPConfig
     */
    protected $spConfig;
    protected $userProviderRepository;

    abstract function handle(Request $request);

    function setUserProviderRepository($provider_repository){
        $this->userProviderRepository = $provider_repository;
    }

    /**
     * @param $email
     * @param $password
     * @return BaseUserInterface|bool|null
     */
    protected function checkLogin($email,$password){
        $doLoginAction = new DoLogin($email,$password, $this->userProviderRepository);
        $user    = $doLoginAction->run();
        return $user;
    }

    /**
     * @param $SAML
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    protected function redirectLoginSAMLRequest($SAML){
        if($this->user && $SAML){
            $decoded = base64_decode($SAML);
            $xml = gzinflate($decoded);

            $deserializationContext = new \LightSaml\Model\Context\DeserializationContext();
            $deserializationContext->getDocument()->loadXML($xml);

            $authnRequest = new AuthnRequest();
            $authnRequest->deserialize($deserializationContext->getDocument()->firstChild, $deserializationContext);

            // ensure $this->user must exist to build SAML Response
            try{
                $issuer_entity_id    = $authnRequest->getIssuer()->getValue();

                $buildSAMLResposnse         = new BuildLoginSAMLResponse($authnRequest, $this->user, $issuer_entity_id);
                $buildSAMLResposnseHTML     = $buildSAMLResposnse->run();

                $data['httpContent'] = $buildSAMLResposnseHTML;
            }catch (\Exception $e){
                Log::error($e->getMessage().'-'.$e->getFile().'-'.$e->getLine());
                throw new \Exception("There is something wrong when sending request");
            }
            return view('http-content',$data);
        }else{
            throw new \Exception("Credential is not valid");
        }
    }



}