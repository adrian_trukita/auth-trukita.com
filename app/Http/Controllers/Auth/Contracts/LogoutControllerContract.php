<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/8/2019
 * Time: 11:51 AM
 */

namespace App\Http\Controllers\Auth\Contracts;


use App\Action\Auth\BuildLogoutSAMLResponse;
use App\Model\Contract\BaseInterfaces\BaseUserInterface;
use Illuminate\Support\Facades\Log;
use LightSaml\Model\Protocol\LogoutRequest;

abstract class LogoutControllerContract
{
    /**
     * @var $user BaseUserInterface
     */
    protected $user;

    abstract protected function setLogoutUser(BaseUserInterface $user);

    /**
     * @param $SAML
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    protected function redirectLogoutSAMLRequest($SAML){
        $decoded = base64_decode($SAML);
        $xml = gzinflate($decoded);

        try{
            $deserializationContext = new \LightSaml\Model\Context\DeserializationContext();
            $deserializationContext->getDocument()->loadXML($xml);

            $logoutRequest = new LogoutRequest();
            $logoutRequest->deserialize($deserializationContext->getDocument()->firstChild, $deserializationContext);

            $buildSAMLResposnse = new BuildLogoutSAMLResponse($logoutRequest, $this->user);
            $target_url         = $buildSAMLResposnse->run();

            $iframe_hidden = [];
            foreach ($target_url as $url){
                $iframe_data['url'] = $url;
                $iframe_hidden[] = view('iframe-http',$iframe_data)->render();
            }

            $logout_callback = route('auth.internal.login');
            if($this->user && $this->user->getIdentity() === "common_user_app"){
                $logout_callback    = route('auth.login');
            }
            $script_appended = '
            <script>
                var iframes = document.getElementsByTagName("iframe");
                iframes[0].onload = function(){
                    window.location.href = "'.$logout_callback.'"
                }
            </script>
            ';
            $data['httpContent']    = implode("<br>",$iframe_hidden).$script_appended;
            return view('http-content',$data);
        }catch (\Exception $e){
            Log::error($e->getMessage().'-'.$e->getFile().'-'.$e->getLine());
            throw new \Exception("Something wrong when trying to logout");
        }catch (\Throwable $e){
            Log::error($e->getMessage().'-'.$e->getFile().'-'.$e->getLine());
            throw new \Exception("Something wrong when trying to logout - Throwable");
        }

    }
}