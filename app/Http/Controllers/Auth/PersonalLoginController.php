<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/27/2018
 * Time: 1:48 PM
 */

namespace App\Http\Controllers\Auth;


use App\Action\Auth\DoLogin;
use App\Action\Auth\GenerateRoleScope;
use App\Action\Auth\GenerateUserToken;
use App\Http\Controllers\Auth\Contracts\LoginControllerContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalLoginController extends LoginControllerContract
{
    public function __construct()
    {
        $this->setUserProviderRepository("users");
    }
    function handle(Request $request)
    {
        // TODO: Implement handle() method.
        if(! Auth::check()){
            $email = $request->email;
            $password = $request->password;
            $doLoginAction = new DoLogin($email,$password);
            $user    = $doLoginAction->run();

            if($user){
                Auth::loginUsingId($user->id);
                $generateUserTokenAction = new GenerateUserToken($user);
                $token = $generateUserTokenAction->run();
                session()->put('access_token',$token);
                return redirect("/");

            }else{
                return redirect(route('auth.login',["error_msg"=>"Credential is not valid"]));
            }
        }else{
            return redirect("/");
        }
    }

}