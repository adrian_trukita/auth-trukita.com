<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/27/2018
 * Time: 2:55 PM
 */

namespace App\Http\Controllers\ShippingPriceReference;


use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ResourceController
{
    protected $client;

    public function __construct()
    {
        $this->client   = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('url_resource')['operation'],
            // You can set any number of default request options.
//            'timeout'  => 4.0,
        ]);
    }
    public function getEstimatedPrice(Request $request){
        $form_param = $request->all();

        $param  = [
            'headers' => [
                'Accept'     => 'application/json',
            ],
            'form_params' => $form_param
        ];

        $response = $this->client->request('POST', 'public/direct-listen/v1/get-shipping-price', $param);

        return $response->getBody();
    }
}