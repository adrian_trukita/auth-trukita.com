<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/6/2018
 * Time: 11:22 AM
 */

namespace App\ObjectClass;


use Illuminate\Support\Facades\Log;

class SPConfig
{
    protected $exist;
    protected $login_destination;
    protected $logout_destination;
    protected $access_token_destination;
    protected $issuer;
    protected $cert_path;
    protected $key;
    protected $config_name;


    public function __construct($app_identity, $entity_id)
    {
        $saml_config = config('saml-config')[$app_identity][$entity_id];

        $this->exist = false;
        try{
            if($saml_config){
                $saml_config_name   = $saml_config['name'];
                $login_destination  = $saml_config['login'];
                $logout_destination = $saml_config['logout'];
//                $access_token       = $saml_config['access_token'];
//                $issuer             = url()->to('saml2/acs');
                $issuer             = url()->to('/');

                $cert   = storage_path('app/saml/'.$saml_config_name.'/sso.cert');
                $key    = file_get_contents(storage_path('app/saml/'.$saml_config_name.'/sso.key'));

                $this->login_destination    = $login_destination;
                $this->logout_destination   = $logout_destination;
//                $this->access_token_destination = $access_token;
                $this->issuer       = $issuer;
                $this->cert_path    = $cert;
                $this->key          = $key;
                $this->config_name  = $saml_config_name;
                $this->exist        = true;
            }
        }catch (\Exception $e){
            Log::info($e->getMessage());
        }


    }

    /**
     * @return bool
     */
    public function isExist(){
        return $this->exist;
    }

    /**
     * @return string
     */
    public function getConfigName(){
        return $this->config_name;
    }

    /**
     * @return string
     */
    public function getLoginDestination()
    {
        return $this->login_destination;
    }

    /**
     * @return string
     */
    public function getLogoutDestination()
    {
        return $this->logout_destination;
    }

    /**
     * @return string
     */
    public function getAccessTokenDestination(){
        return $this->access_token_destination;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * @return string
     */
    public function getCertPath(): string
    {
        return $this->cert_path;
    }

    /**
     * @return bool|string
     */
    public function getKey()
    {
        return $this->key;
    }


}