<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendResetPassword extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    protected $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($token,$name)
    {
        $this->token = $token;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Reset Password')
                    ->greeting('Dear, '.$this->name)
                    ->line('Kami mendeteksi permintaan reset password untuk akun Anda.')
                    ->line('Silahkan reset password akun Anda dengan klik tombol dibawah ini')
                    ->action('Reset Password', route('auth.reset-password-confirmation', ['token' => $this->token]))
                    ->line('Jika Anda tidak merasa meminta reset password, abaikan email ini.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
