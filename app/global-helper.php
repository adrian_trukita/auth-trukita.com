<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 8:20 PM
 */

/**
 * Logging exception to readable string
 * @param Exception $e
 * @return string
 */
function loggingExceptionString(Exception $e){
    return $e->getMessage().'-'.$e->getFile().'-'.$e->getLine().'-'.$e->getPrevious();
}

/**
 * return session message string
 * @return string
 */
function alertBar(){
    $str = "";
    if(session()->get('success_message')){
        $str .= "<h6 style='color:green'>".session('success_message')."</h6>";
    }
    if(session()->get('error_message')){
        $str .= "<h6 style='color:red'>".session('error_message')."</h6>";
    }
    if(isset($errors) && $errors->any()){
        foreach ($errors->all() as $error):
            $str .= "<h6 style='color:red'>".$error."</h6>";
        endforeach;
    }

    return $str;
}