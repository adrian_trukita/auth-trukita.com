<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 12/4/2018
 * Time: 4:14 PM
 */

namespace App\Model;


use App\Models\Contract\UserInterface;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;


class Users extends UserInterface
{
    protected $identity = "common_user_app";
    protected $table = "users";

    protected $hidden = ['password', 'remember_token'];
    protected $fillable = [ 'name', 'email', 'username', 'password', 'status', 'role', 'activation_key', 'api_token'];

    /**
     * Get User Identity
     * @return string
     */
    public function getIdentity()
    {
        // TODO: Implement getIdentity() method.
        return $this->identity;
    }

    /**
     * Custom find user passport - either by email or msisdn
     * @param string $username
     *
     * @return Users
     */
    public function findForPassport($username){
        return $this->where(function($q) use ($username){
            $q->where("email",$username)
                ->orWhere('msisdn',$username);
        })->first();
    }

    /**
     * Custom validate password for passport - either by password field or otp field
     * @param $password
     * @return bool
     */
    public function validateForPassportPasswordGrant($password){
        $return = false;
        if(Hash::check($password,$this->getAuthPassword()) || $this->otp === $password){
            $return = true;
        }
        return $return;
    }
}