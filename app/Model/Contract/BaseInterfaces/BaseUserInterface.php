<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 3:13 PM
 */

namespace App\Model\Contract\BaseInterfaces;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

abstract class BaseUserInterface extends Model implements AuthenticatableContract, AuthorizableContract
{
    abstract function getIdentity();
//    use Authenticatable, Authorizable,HasApiTokens, Notifiable;
    use Authenticatable, Authorizable,HasMultiAuthApiTokens, Notifiable;
}