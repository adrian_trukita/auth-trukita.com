<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    //
    protected $table = "logs";
    protected $primaryKey = "id";

    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];
    protected $guarded = ['deleted_at'];
}
