<?php

namespace App\Model;

use App\Models\Contract\InternalUserInterface;
use App\Models\Contract\UserInterface;
use Illuminate\Database\Eloquent\Model;

class InternalUsers extends InternalUserInterface
{
    //
    protected $identity = "internal_user_app";
    protected $table    = "internal_users";

    protected $hidden = ['password'];
    protected $fillable = [ 'name', 'email', 'username', 'password', 'role'];

    /**
     * Get Internal User App Identity
     * @return string
     */
    public function getIdentity()
    {
        // TODO: Implement getIdentity() method.
        return $this->identity;
    }
}
