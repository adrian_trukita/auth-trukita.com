<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 1/1/2019
 * Time: 9:06 PM
 */

namespace App\Traits;


use App\Repositories\Contract\LogRepositoryInterface;
use App\Repositories\Implementation\LogRepository;
use Illuminate\Http\Request;

/**
 * Trait CanLog
 * @package App\Traits
 */
trait CanLog
{
    public function createLog($type, $onFunction, $description, Request $request, $actor_class="system", $actor_identifier="system")
    {

        $add_param = [
            'type'          => $type,
            'actor_class'   => $actor_class,
            'actor_identifier'   => $actor_identifier,
            'onFunction'    => $onFunction,
            'description'   => $description,

            'isJson'        => $request->isJson(),
            'expectsJson'   => $request->expectsJson(),
            'wantsJson'     => $request->wantsJson(),
            'header'        => gettype($request->header()) == 'array'? serialize($request->header())
                : (string) $request->header(),
            'querystring'   => gettype($request->query()) == 'array'? serialize($request->query())
                : (string) $request->query(),
            'method'        => $request->method(),
            'ip'            => $request->ip(),
            'ips'           => serialize($request->ips()),
            'userAgent'     => serialize($request->userAgent()),
            'fingerprint'   => serialize($request->fingerprint()),
        ];
        $log_repository = new LogRepository();
        $log_repository->addLogs($add_param);
    }
}