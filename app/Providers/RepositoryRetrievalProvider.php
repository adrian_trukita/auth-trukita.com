<?php

namespace App\Providers;

use App\Repositories\Contract\LogRepositoryInterface;
use App\Repositories\Contract\UserRepositoryInterface;
use App\Repositories\Implementation\LogRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryRetrievalProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(UserRepositoryInterface::class,function(){
           return new UserRepository();
        });

        $this->app->singleton(LogRepositoryInterface::class,function(){
            return new LogRepository();
        });
    }
}
