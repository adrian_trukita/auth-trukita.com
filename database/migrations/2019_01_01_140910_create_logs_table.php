<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('actor_class');
            $table->string('actor_identifier');

            $table->string('onFunction');
            $table->text('description');

            $table->boolean('isJson')->nullable();
            $table->boolean('expectsJson')->nullable();
            $table->boolean('wantsJson')->nullable();
            $table->text('header')->nullable();
            $table->text('querystring')->nullable();
            $table->string('method',250)->nullable();
            $table->string('ip',191)->nullable();
            $table->string('ips',250)->nullable();
            $table->string('userAgent',250)->nullable();
            $table->string('fingerprint',191)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
