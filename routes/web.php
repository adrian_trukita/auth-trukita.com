<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return view('welcome');
});

Route::get('/internal-dashboard-panel',function(){
    return view('dashboard/internal-dashboard-panel');
})->middleware('auth:admin');


Route::group(['namespace' => 'Auth'], function () {
    Route::group(['prefix' => 'internal-admin','namespace'  => 'Internal'], function () {
        Route::get('/login',["as" => "auth.internal.login",
            "uses" => "LoginController@view"
        ]);
        Route::get('/logout',["as" => "auth.internal.logout",
            "uses" => "LogoutController@handle"
        ]);

        Route::get('/direct-login',["as" => "auth.internal.direct-login",
            "uses" => "LoginController@directCheck"
        ]);

        Route::post('/do-login',["as" => "auth.internal.login.post",
            "uses" => "LoginController@handle"
        ]);

        Route::get('/serve-access-token',["as"    => "serve.access.token",
            "uses"  => "AccessTokenController@serve"
        ])->middleware("auth:admin");

        Route::post('/do-personal-login',["as" => "auth.internal.personal-login.post",
            "uses" => "PersonalLoginController@handle"
        ]);

        Route::get('/personal-logout',["as" => "auth.internal.personal-logout",
            "uses" => "PersonalLogoutController@handle"
        ]);
    });


    Route::get('/login',["as" => "auth.login",
        "uses" => "LoginController@view"
    ]);
    Route::get('/logout',["as" => "auth.logout",
        "uses" => "LogoutController@handle"
    ]);
    Route::get('/direct-login',["as" => "auth.direct-login",
        "uses" => "LoginController@directCheck"
    ]);
    Route::post('/do-login',["as" => "auth.login.post",
        "uses" => "LoginController@handle"
    ]);

    Route::post('/do-personal-login',["as" => "auth.personal-login.post",
        "uses" => "PersonalLoginController@handle"
    ]);
    Route::get('/personal-logout',["as" => "auth.personal-logout",
        "uses" => "PersonalLogoutController@handle"
    ]);

    Route::get('/forget-password',["as"    => "auth.forget-password",
        "uses"  => "ForgotPasswordController@view"
    ]);
    Route::post('/request-forget-password',["as"    => "auth.request-forget-password.post",
        "uses"  => "ForgotPasswordController@handle"
    ]);
    Route::get('/reset-password/confirmation/{token}', [ 'as' => 'auth.reset-password-confirmation',
        'uses' => 'ResetPasswordController@view'
    ]);
    Route::post('/do-reset-password/confirmation/{token}', [ 'as' => 'auth.reset-password-confirmation.post',
        'uses' => 'ResetPasswordController@handle'
    ]);

    Route::get('/serve-access-token',["as"    => "serve.access.token",
        "uses"  => "AccessTokenController@serve"
    ]);
});




//Route::get('/home', 'HomeController@index')->name('home');
