<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//

Route::group(['prefix' => 'v1'], function () {

    /**
     * Auth Route
     * Already handled by OAuth2
     */


    /**
     * API Route
     */
    Route::group(['middleware'=> 'auth:api','namespace' => 'ShippingPriceReference'], function () {
        Route::post('/request-route-price',[
            "as" => "api.requestRoutePrice", "uses" => "ResourceController@getEstimatedPrice"
        ])->middleware('scope:route-prices');
    });
});