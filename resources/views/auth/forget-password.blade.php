<form id="form-forget-password" role="form"  method="POST" action="{{ route('auth.request-forget-password.post') }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="form-label" for="forget-password-email">Alamat Email</label>
        <input id="forget-password-email" name="email" class="form-input" type="email" placeholder=" " required/>
        <span class="hidden clear-form cursor-pointer">&times;</span>
    </div>
    <button type="submit" class="btn btn-blue btn-block mb-4 mt-5">Kirim Email Verifikasi</button>
</form>
<div class="text-center">
    <small>Sudah Memiliki Akun?</small><br>
    <small><a class="text-dark" href="{{route('auth.login')}}"><b>Masuk Sekarang</b></a></small><br>
</div>