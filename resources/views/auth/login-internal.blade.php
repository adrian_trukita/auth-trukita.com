


<h4>Internal Login</h4>
{{alertBar()}}
@if(isset($samlRequest))
    <form action="{{route('auth.internal.login.post')}}" method="POST">
        <input type="hidden" id="SAMLRequest" name="SAMLRequest" value="{{ $samlRequest }}">
@else
    <form action="{{route('auth.internal.personal-login.post')}}" method="POST">
@endif
    @if(isset($_GET['error_msg']))
        {{$_GET['error_msg']}}
    @endif
    {!! csrf_field() !!}

    <input type="text" name="email">
    <input type="password" name="password">
    <button type="submit">Submit</button>
</form>

