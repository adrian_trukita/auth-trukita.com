<!-- START Forget Password Form -->

<h4>Reset Password Confirmation</h4>
<form id="form-reset-password-confirmation" class="p-t-15" role="form"  method="POST" action="{{ route('auth.reset-password-confirmation.post', request()->segment(3)) }}">
{{ csrf_field() }}
<!-- START Form Control-->
    {{alertBar()}}

    <div class="form-group">
        <label>New Password</label>
        <input type="password" name="password" placeholder="New Password" id="password" class="form-control pg-rpc__password" required>
    </div>
    <div class="form-group">
        <label>Re-type New Password</label>
        <input type="password" name="re-password" placeholder="Re-type New Password" id="re-password" class="form-control pg-rpc__password" required>
    </div>
    <br/>
    <div class="flex flex-set-center">
        <button class="btn-stylish font-xs" type="submit">CONFIRM CHANGE PASSWORD</button>
    </div>
    <div class="text-center"></br>
        <span>Have an account? <br/>
			<a class="font-700 font-c-semi-black" href="{{ route('auth.login') }}">Sign In </a> </span>
    </div>
</form>
<!--END Login Form-->
<!--END Login Form-->