<?php
    $endpoint = route('auth.login.post');
    if(! isset($samlRequest))
    {
        $endpoint = route('auth.personal-login.post');
    }
?>
{{alertBar()}}

<h4>Login</h4>
<form action="{{$endpoint}}" method="POST">
@if(isset($samlRequest))
    <input type="hidden" id="SAMLRequest" name="SAMLRequest" value="{{ $samlRequest }}">
@endif
    @if(isset($_GET['error_msg']))
        {{$_GET['error_msg']}}
    @endif
    {!! csrf_field() !!}

    <input type="text" name="email">
    <input type="password" name="password">
    <button type="submit">Submit</button>
</form>

<a href="{{route('auth.forget-password')}}">Forget password</a>

